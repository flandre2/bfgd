use eyre::{Result, bail, WrapErr};
use safe_transmute::{
    guard::SingleManyGuard,
    transmute_one,
};
use base64::{
    decode_config_slice,
    Config,
    CharacterSet
};

struct Peer {
    id: u64,
}
impl Peer {
    fn new<T: Clone + AsRef<[u8]>>(pubkey: T) -> Result<Self> {
        let mut pubkey_bin: [u8;32] = [0;32];
        decode_config_slice(pubkey,
                            Config::new(CharacterSet::Standard, true),
                            &mut pubkey_bin)
            .wrap_err("base64 decoding error")?;
        Ok(Peer {
            id: transmute_one::<u64>(&pubkey_bin)?,
        })
    }
}

fn main() -> Result<()> {
    Ok(())
}
